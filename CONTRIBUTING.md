## Introduction

Contributing to SOB translation project can be done in multiple easy ways. The easiest is to clone the repo into your own gitlab repo and edit the machine translated text there. Then submit a merge request to the main repo and wait for a merge approval.


## General

From Jan 14, 2019 on, we will only work on the text files inside the 'cheats' directory.

We ask everyone to follow these short standards:

## Coding standards
* Do take note of using ' in your text. A single ' will be interpreted by the program, so a ' in your text needs to be "escaped" by a second one.

` I didn't do that! ` will generate an error

` I didn''t do that! ` is correct use.

* Dialog text should also be surrounded by single double quotes. Since it is hard to follow the text sometimes with dashes or none at all.

` "How are you today?" I asked her. ` is correct usage.

` ""How are you today?"" I asked her. ` will generate two double quotes within the game.

` - How are you today? - I asked her. ` is what we are trying to eliminate.

* Thoughts. 

Thoughts are not spoken out loud. To distinguish these from text audible to others we encapsulated the text with « ... »

`«What a week» I thought ` is correct

`"What a week" I thought` is incorrect

* Only one character should be considered your aunt. Nina, the rest of the characters shouldn't be referred to as aunt.


## Interpretation
As the machine translated text is sometimes hard to follow, it is ok to do some interpretation. But please try to follow a consistent interpretation of all the text.